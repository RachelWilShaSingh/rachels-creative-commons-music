# Rachel's Creative Commons Music

## About

I like to make songs for my own games and videos and such.
I also like to not get copyright striked on videos,
thus the effort to make my own music.

This is also a gift for you all so that you, too, can avoid
YouTube copyright strikes. ;)

Feel free to use this stuff for anything!
I'd like to be credited but you don't have to,
I'm basically releasing all this music into the public domain.

If you want to donate to me to show your appreciation,
you're welcome to buy me a coffee: https://ko-fi.com/rachelwilshasingh

## License

All this music is public domain, yo.
The repository just says "Creative Commons" because
I figured it'd be easier to find the songs,
'cuz usually public domain music is from the 1920s and earlier.
