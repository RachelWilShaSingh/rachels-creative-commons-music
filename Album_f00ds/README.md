# f00dz album (2024)

Sometimes I think to myself, "gee, I need to make more music so I have more stuff to choose from to use in my games/videos/whatever."

Another time I thought to myself, "I want to make a song called 'beans'."

And thus this project was born in order to create 26 songs over the course of 2024, and of course you're welcome to use this stuff as well. :)
